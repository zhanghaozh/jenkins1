package com.example.jenkins.demo.controller;

import com.example.jenkins.demo.server.demoServer;
import com.example.jenkins.demo.server.demoServerImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("hello")
public class demoController {
    @Resource
    private demoServerImpl demo;

    @GetMapping("demo")
    public String hello(){
        return demo.helloWorld();
    }
}
